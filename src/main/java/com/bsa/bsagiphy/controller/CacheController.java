package com.bsa.bsagiphy.controller;

import com.bsa.bsagiphy.model.Cache;
import com.bsa.bsagiphy.service.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/cache")
public class CacheController {

    private final CacheService cacheService;

    @Autowired
    public CacheController(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @GetMapping
    public ResponseEntity<List<Cache>> getCache(@RequestParam String query) {
        List<Cache> allCache = cacheService.getAllCache(query);
        if (allCache.get(0).getGifs().isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(allCache);
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteCache() throws IOException {
        cacheService.deleteCache();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
