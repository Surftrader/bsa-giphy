package com.bsa.bsagiphy.service;

import com.bsa.bsagiphy.model.Cache;
import com.bsa.bsagiphy.repository.CacheRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class CacheService {

    private CacheRepository cacheRepository;

    @Autowired
    public CacheService(CacheRepository cacheRepository) {
        this.cacheRepository = cacheRepository;
    }

    public List<Cache> getAllCache(String query) {
        return cacheRepository.getCache(query);
    }

    public void deleteCache() throws IOException {
        cacheRepository.deleteCache();
    }
}
